using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Unity.Mvc4;
using MeNotify.Controllers;
using MeNotify.Repository;
using Moq;

namespace MeNotify
{
  public static class Bootstrapper
  {
    public static IUnityContainer Initialise()
    {
      var container = BuildUnityContainer();

      DependencyResolver.SetResolver(new UnityDependencyResolver(container));

      return container;
    }

    private static IUnityContainer BuildUnityContainer()
    {
      var container = new UnityContainer();

      // register all your components with the container here
      // it is NOT necessary to register your controllers

      // e.g. container.RegisterType<ITestService, TestService>();
     

      
      container.RegisterType<IController, HomeController>("Home");

      
      RegisterTypes(container);

      return container;
    }

    public static void RegisterTypes(IUnityContainer container)
    {
        container.RegisterType<IRepository, ErrorRepository>();
        //var mock=new Mock<IRepository>();
        //container.RegisterInstance<IRepository>(mock.Object);
    }
  }
}