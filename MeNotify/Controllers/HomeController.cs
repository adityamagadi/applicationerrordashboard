﻿#region using
using MeNotify.Models;
using MeNotify.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
#endregion
namespace MeNotify.Controllers
{
    /// <summary>
    /// Home Controller manages the logic for getting error messages.
    /// </summary>
    public class HomeController : Controller
    {
        public const string Web_Service = "WebService";
        public const string Windows_Error = "WindowsService";
        public const string WebUI_Error = "WebApplication";

        private IRepository repository;
        /// <summary>
        /// Home controller constructor.
        /// </summary>
        /// <param name="_repository">Repository interface</param>
        public HomeController(IRepository _repository)
        {
            this.repository = _repository;
        }
        /// <summary>
        /// Default constructor
        /// </summary>
        public HomeController()
        {

        }
        public ActionResult Index()
        {
            

            return View();
        }
        /// <summary>
        /// Method to return count of error for each day in a week
        /// </summary>
        /// <param name="data">Error message data</param>
        /// <returns>dictionary containing eror for each day of the week</returns>
        public Dictionary<string, int> GetWebServiceCountForErrors(List<ErrorModel> data)
        {
            Dictionary<string, int> dictionary = new Dictionary<string, int>();

            dictionary.Add(DateTime.UtcNow.AddDays(0).DayOfWeek.ToString(), data.Where(x => x.ChangeByDate.DayOfWeek == DateTime.UtcNow.AddDays(0).DayOfWeek).ToList().Count);
            dictionary.Add(DateTime.UtcNow.AddDays(-1).DayOfWeek.ToString(), data.Where(x => x.ChangeByDate.DayOfWeek == DateTime.UtcNow.AddDays(-1).DayOfWeek).ToList().Count);
            dictionary.Add(DateTime.UtcNow.AddDays(-2).DayOfWeek.ToString(), data.Where(x => x.ChangeByDate.DayOfWeek == DateTime.UtcNow.AddDays(-2).DayOfWeek).ToList().Count);
            dictionary.Add(DateTime.UtcNow.AddDays(-3).DayOfWeek.ToString(), data.Where(x => x.ChangeByDate.DayOfWeek == DateTime.UtcNow.AddDays(-3).DayOfWeek).ToList().Count);
            dictionary.Add(DateTime.UtcNow.AddDays(-4).DayOfWeek.ToString(), data.Where(x => x.ChangeByDate.DayOfWeek == DateTime.UtcNow.AddDays(-4).DayOfWeek).ToList().Count);
            dictionary.Add(DateTime.UtcNow.AddDays(-5).DayOfWeek.ToString(), data.Where(x => x.ChangeByDate.DayOfWeek == DateTime.UtcNow.AddDays(-5).DayOfWeek).ToList().Count);
            dictionary.Add(DateTime.UtcNow.AddDays(-6).DayOfWeek.ToString(), data.Where(x => x.ChangeByDate.DayOfWeek == DateTime.UtcNow.AddDays(-6).DayOfWeek).ToList().Count);
            return dictionary;
        }

        /// <summary>
        /// Gets all error message of the current week
        /// </summary>
        /// <returns>Json object containing all the errors of the current week</returns>
        public JsonResult GetMessages()
        {
            var logDataList = from error in repository.GetLogData() select error;


            var webUIErrorData = new List<ErrorModel>();
            var webServiceErrorData = new List<ErrorModel>();
            var windowsServiceErrorData = new List<ErrorModel>();
            

            windowsServiceErrorData = logDataList.Where(x => x.Category == Windows_Error).ToList();
            webServiceErrorData = logDataList.Where(x => x.Category == Web_Service).ToList();
            webUIErrorData = logDataList.Where(x => x.Category == WebUI_Error).ToList();

            var webUIErrorCount = webUIErrorData.Count;
            var windowsServiceErrorCount = windowsServiceErrorData.Count;
            var webServiceErrorCount = webServiceErrorData.Count;

            Dictionary<string, int> dictionaryWebServiceErrors = GetWebServiceCountForErrors(webServiceErrorData);
            Dictionary<string, int> dictionaryWindowsErrors = GetWebServiceCountForErrors(windowsServiceErrorData);
            Dictionary<string, int> dictionarWebErrors = GetWebServiceCountForErrors(webUIErrorData);

            var ErrorData = new
            {
                WeekErrorDataWebService = dictionaryWebServiceErrors,
                WeekErrorDataWindowsService = dictionaryWindowsErrors,
                WeekErrorDataWeb = dictionarWebErrors,
                errors = logDataList,
                WebUIErrors = webUIErrorData,
                WindowsServiceErrors = windowsServiceErrorData,
                WebServiceErrors = webServiceErrorData,
                UIErrorCount = webUIErrorCount,
                WinServiceCount = windowsServiceErrorCount,
                WebserErrorCount = webServiceErrorCount
            };
            JsonResult result = new JsonResult()
            {
                Data = ErrorData,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
            return result;

        }


    }
}
