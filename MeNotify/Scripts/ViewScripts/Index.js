﻿var Page = $(function () {
    // Declare a proxy to reference the hub.
    var notifications = $.connection.messageHub;
    //debugger;
    // Create a function that the hub can call to broadcast messages.
    notifications.client.updateMessages = function () {
        getAllMessages()

    };
    // Start the connection.
    $.connection.hub.start().done(function () {
        alert("connection started");
        var data = getAllMessages();

    }).fail(function (e) {
        alert(e);
    });
});

function displayCharts(webservice, windowsservice, web) {
    var s1 = [];
    var s2 = [];
    var s3 = [];
    var webserviceErrorCount = 0;
    var windowserviceErrorCount = 0;
    var webUIErrorsCount = 0;
    for (var key in webservice) {
        if (webservice.hasOwnProperty(key)) { // this will check if key is owned by data object and not by any of it's ancestors
            s1.push(webservice[key]); // this will show each key with it's value
        }
    }
    for (var key in windowsservice) {
        if (windowsservice.hasOwnProperty(key)) { // this will check if key is owned by data object and not by any of it's ancestors
            s2.push(windowsservice[key]); // this will show each key with it's value
        }
    }
    for (var key in web) {
        if (web.hasOwnProperty(key)) { // this will check if key is owned by data object and not by any of it's ancestors
            s3.push(web[key]); // this will show each key with it's value
        }
    }
    // Can specify a custom tick Array.
    // Ticks should match up one for each y value (category) in the series.
    var ticks = ['Monday', 'Tuesday', 'Wednsday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];

    plot1 = $.jqplot('chart1', [s1, s2, s3], {
        height: 200,
        width: 300,
        // The "seriesDefaults" option is an options object that will
        // be applied to all series in the chart.
        seriesDefaults: {
            renderer: $.jqplot.BarRenderer,
            rendererOptions: { fillToZero: true }
        },
        // Custom labels for the series are specified with the "label"
        // option on the series option.  Here a series option object
        // is specified for each series.
        series: [
            { label: 'WebService Errors' },
            { label: 'Windows Service Errors' },
            { label: 'UI Errors' }
        ],
        // Show the legend and put it outside the grid, but inside the
        // plot container, shrinking the grid to accomodate the legend.
        // A value of "outside" would not shrink the grid and allow
        // the legend to overflow the container.
        legend: {
            show: true,
            placement: 'outsideGrid'
        },
        axes: {
            // Use a category axis on the x axis and use our custom ticks.
            xaxis: {
                renderer: $.jqplot.CategoryAxisRenderer,
                ticks: ticks
            },
            // Pad the y axis just a little so bars can get close to, but
            // not touch, the grid boundaries.  1.2 is the default padding.
            yaxis: {

                tickOptions: { formatString: '%d' }
            }
        }
    });

    
    for (var key in s1) {
        webserviceErrorCount += s1[key];
    }
    for (var key in s2) {
        windowserviceErrorCount += s2[key];
    }
    for (var key in s3) {
        webUIErrorsCount += s3[key];
    }
    var pieChartData = [webUIErrorsCount, windowserviceErrorCount, webserviceErrorCount];

    var plot2 = $.jqplot('myChart1', [[['Web Service Errors', webserviceErrorCount], ['Windows Service Errors', windowserviceErrorCount], ['Web UI Errors', webUIErrorsCount]]],
        {
            seriesDefaults: {
                renderer: jQuery.jqplot.PieRenderer,
                rendererOptions: {
                    padding: 2,
                    sliceMargin: 2,
                    showDataLabels: true
                }
            },
            legend: { show: true, location: 'e' }
        });
    $("#tabs").tabs({
        activate: function (event, ui) {
            if (ui.index === 1 && plot1._drawCount === 0 && plot2._drawCount === 0) {
                plot1.replot();

            }
        }
    });
}
function displayGrid(result) {
    $("#tblJQGrid").jqGrid(

    {
        data: result.webServiceErrors,
        datatype: "local",
        rowList: [10, 20, 30],
        colNames: ['LogId', 'LogMsg', 'ChangeByUser', 'ChangeByDate', 'Category'],
        colModel: [
        { name: 'LogId', index: 'LogId', width: 150, sorttype: "int" },
        { name: 'LogMsg', index: 'LogMsg', width: 300, sortable: false },
         { name: 'ChangeByUser', index: 'ChangeByUser', width: 150, sortable: false },
        { name: 'ChangeByDate', index: 'ChangeByDate', width: 100, sortable: false, formatter: "date" },
         { name: 'Category', index: 'Category', width: 150, sortable: false }],
        rowNum: 10,

        viewrecords: true,
        sortorder: "desc",
        caption: "Web Service Errors",
        scrollOffset: 0,
        pager: "#plist47"
    });


    $("#tblJQGrid1").jqGrid(

    {
        data: result.WindowsServiceErrors,
        datatype: "local",
        rowList: [10, 20, 30],
        colNames: ['LogId', 'LogMsg', 'ChangeByUser', 'ChangeByDate', 'Category'],
        colModel: [
        { name: 'LogId', index: 'LogId', width: 150, sorttype: "int" },
        { name: 'LogMsg', index: 'LogMsg', width: 300, sortable: false },
         { name: 'ChangeByUser', index: 'ChangeByUser', width: 150, sortable: false },
        { name: 'ChangeByDate', index: 'ChangeByDate', width: 100, sortable: false, formatter: "date" },
         { name: 'Category', index: 'Category', width: 150, sortable: false }],
        rowNum: 10,

        viewrecords: true,
        sortorder: "desc",
        caption: "Windows Service Errors",
        scrollOffset: 0,
        pager: "#plist471"
    });

    $("#tblJQGrid2").jqGrid(

    {
        data: result.WebUIErrors,
        datatype: "local",
        rowList: [10, 20, 30],
        colNames: ['LogId', 'LogMsg', 'ChangeByUser', 'ChangeByDate', 'Category'],
        colModel: [
        { name: 'LogId', index: 'LogId', width: 150, sorttype: "int" },
        { name: 'LogMsg', index: 'LogMsg', width: 300, sortable: false },
         { name: 'ChangeByUser', index: 'ChangeByUser', width: 150, sortable: false },
        { name: 'ChangeByDate', index: 'ChangeByDate', width: 100, sortable: false, formatter: "date" },
         { name: 'Category', index: 'Category', width: 150, sortable: false }],
        rowNum: 10,

        viewrecords: true,
        sortorder: "desc",
        caption: "Web Errors",
        scrollOffset: 0,
        pager: "#plist472"
    });
}

function getAllMessages() {
    var tbl = $('#messagesTable');
    $.ajax({
        url: '/home/getmessages',
        contentType: 'application/json ; charset:utf-8',
        type: 'GET',
        dataType: 'json'
    }).success(function (result) {
        $("#chart1").empty();
        displayCharts(result.WeekErrorDataWebService, result.WeekErrorDataWindowsService, result.WeekErrorDataWeb);

        // $("#tblJQGrid").clearGridData();
        //$("#tblJQGrid").addJSONData(result.webServiceErrors);

        //dataWeb = result.webServiceErrors;
        displayGrid(result);
        $('#tblJQGrid').jqGrid('setGridParam', { datatype: 'local', data: result.webServiceErrors }).trigger('reloadGrid');
        $('#tblJQGrid2').jqGrid('setGridParam', { datatype: 'local', data: result.WindowsServiceErrors }).trigger('reloadGrid');
        $('#tblJQGrid3').jqGrid('setGridParam', { datatype: 'local', data: result.WebUIErrors }).trigger('reloadGrid');
    }).error(function (error) {
        alert(error);
    });
}

