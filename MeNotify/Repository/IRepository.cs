﻿using MeNotify.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeNotify.Repository
{
    /// <summary>
    /// Interface to get data from repository
    /// </summary>
   public interface IRepository:IDisposable
    {
       /// <summary>
       /// Get logged error data
       /// </summary>
       /// <returns>ErrorModel object</returns>
       IEnumerable<ErrorModel> GetLogData();
    }
}
