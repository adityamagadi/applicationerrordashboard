﻿#region using
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using MeNotify.Hubs;
using Owin;
using MeNotify.Models;
#endregion
namespace MeNotify.Repository
{
    /// <summary>
    /// Repository class for database transactions
    /// </summary>
    public class ErrorRepository : IRepository
    {

        //readonly string connectionstr = "Server=f80c9d2a-0725-4272-b139-a3df0076e35b.sqlserver.sequelizer.com;Database=dbf80c9d2a07254272b139a3df0076e35b;User ID=tlczthzdfpiufmgu;Password=fHfLEVSdiXmwe3fyfx3cumVSQph7AoWpesXtUeaaSbU6fFctE4RWHnA2Uv4pMUK4;";
        readonly string connectionstr = "Server=e8125e04-bf67-4d94-ba4e-a3e5013008e0.sqlserver.sequelizer.com;Database=dbe8125e04bf674d94ba4ea3e5013008e0;User ID=czzwayaqsctcakde;Password=PVzRYRQDXBjXT6KdSJsdo6j6wZe3tEky5wysTedwnxeFWm3UCSwyhc4RKBt8MVkr;";

        /// <summary>
        /// Default constructor
        /// </summary>
        public ErrorRepository()
        {

        }
        /// <summary>
        /// Call back method when dataset changes
        /// </summary>
        /// <param name="sender">sender object</param>
        /// <param name="e">eventargs</param>
        private void dependency_OnChange(object sender, SqlNotificationEventArgs e)
        {
            if (e.Type == SqlNotificationType.Change)
            {
                ErrorMessageHub.SendErrorMessages();
            }
        }

        /// <summary>
        /// Gets all the logged errors
        /// </summary>
        /// <returns></returns>
        IEnumerable<ErrorModel> IRepository.GetLogData()
        {
            var messages = new List<ErrorModel>();
            try
            {

                using (var connection = new SqlConnection(connectionstr))
                {
                    connection.Open();
                    DateTime date = DateTime.UtcNow.AddDays(-7);

                    using (var command = new SqlCommand(@"SELECT LogId,LogMsg,ChangeByUser,ChangebyDate,Category FROM [dbo].[ErrorLogs] WHERE [ChangebyDate] >=@date ORDER BY LogId DESC", connection))
                    {
                        command.Parameters.AddWithValue("@date", date);
                        command.Notification = null;

                        var dependency = new SqlDependency(command);
                        dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);

                        if (connection.State == ConnectionState.Closed)
                            connection.Open();

                        var reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            messages.Add(item: new ErrorModel { LogId = Convert.ToInt32(reader["LogId"]), LogMsg = reader["LogMsg"].ToString(), ChangeByUser = reader["ChangeByUser"].ToString(), ChangeByDate = Convert.ToDateTime(reader["ChangebyDate"]), Category = reader["Category"].ToString() });
                        }
                    }

                }
                return messages;
            }
            catch (Exception e)
            {
                messages = null;
                return messages;
            }


        }

        void IDisposable.Dispose()
        {

        }
    }
}