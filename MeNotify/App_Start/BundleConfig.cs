﻿using System.Web;
using System.Web.Optimization;

namespace MeNotify
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-1.11.1.js"
                          ));

            bundles.Add(new ScriptBundle("~/bundles/Index").Include(
                        "~/Scripts/jquery.signalR-2.1.2.js",
                         "~/Scripts/jqplot/excanvas.js",
                         "~/Scripts//jqplot/excanvas.min.js",
                         "~/Scripts/jqplot/jquery.jqplot.js",
                          "~/Scripts/jqplot/plugins/jqplot.barRenderer.js",
                            "~/Scripts/jqplot/plugins/jqplot.categoryAxisRenderer.js",
                            "~/Scripts/plugins/jqplot.pieRenderer.js",
                             "~/Scripts/plugins/jqplot.dateAxisRenderer.js",
                             "~/Scripts/jqplot/plugins/jqplot.pointLabels.js",
                            "~/Scripts/i18n/grid.locale-en.js",
                             "~/Scripts/jquery.jqGrid.js",
                         "~/signalr/hubs",
                         "~/Scripts/ViewScript/ViewScript.Index.js"
                          ));


            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/themes/Site.css", "~/Content/jqplot/jquery.jqplot.css", "~/Content/jquery.jqGrid/ui.jqgrid.css"));


            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                "~/Content/themes/jqueryui/jquery-ui.css",
                 "~/Content/themes/jqueryui/jquery-ui.structure.css",
                  "~/Content/themes/jqueryui/jquery-ui.theme.css"
             ));
        }
    }
}