﻿#region using
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
#endregion
namespace MeNotify.Hubs
{
    /// <summary>
    /// Hub class for notifying error messages
    /// </summary>
    public class ErrorMessageHub: Hub
    {
        
        public void Hello()
        {
            Clients.All.hello();
        }
        /// <summary>
        /// Hub method to send error message to all clients
        /// </summary>
        [HubMethodName("sendErrorMessages")]
        public static void SendErrorMessages()
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<ErrorMessageHub>();
            context.Clients.All.updateErrorMessages();
        }
    }
}