﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MeNotify.Repository;
using MeNotify.Models;
using System.Web.Mvc;
using MeNotify.Controllers;
using System.Collections.Generic;

namespace UnitTestProject2
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            var repository = new Mock<IRepository>();
            var list = new List<ErrorModel>();
            ErrorModel map = new ErrorModel()
            {
                 LogId=1,
                 Category="test"
            };

            list.Add(map);

            repository.Setup(x => x.GetLogData()).Returns(list);
            var controller = new HomeController(repository.Object);
            //Act
            var result = controller.GetMessages() as JsonResult;

            //Assert
            //Assert.AreEqual(1, result.Data);

        }
    }
}
